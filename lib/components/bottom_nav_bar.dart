import 'package:f_access/typo/typo_colors.dart';
import 'package:f_access/views/assistant.dart';
import 'package:flutter/material.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({super.key});

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        constraints: const BoxConstraints(maxHeight: 96),
        decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(width: 16, color: Colors.white),
            bottom: BorderSide(width: 16, color: Colors.white),
          ),
        ),
        child: InkWell(
          onTap: () {
            Route createRoute() {
              return PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    const Assistant(),
                transitionsBuilder:
                    (context, animation, secondaryAnimation, child) {
                  final tween = Tween(begin: 0.0, end: 1.0);
                  return FadeTransition(
                    opacity: animation.drive(tween),
                    child: child,
                  );
                },
              );
            }

            Navigator.push(context, createRoute());
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text("Hey Sarah!",
                  style: TextStyle(fontSize: 20, color: TypoColors.black2)),
              const SizedBox(width: 16),
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        gradient: TypoColors.gradientMain,
                        borderRadius: BorderRadius.circular(32)),
                    constraints: const BoxConstraints(
                      minHeight: 64,
                      minWidth: 64,
                    ),
                  ),
                  Hero(
                    tag: "assistant-icon",
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 52,
                          width: 52,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(32),
                            color: TypoColors.black1,
                          ),
                        ),
                        ShaderMask(
                          blendMode: BlendMode.srcIn,
                          shaderCallback: (bounds) =>
                              TypoColors.gradientMain.createShader(bounds),
                          child: const Icon(Icons.mic_rounded, size: 32),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
