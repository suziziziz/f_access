import 'package:f_access/components/bottom_nav_bar.dart';
import 'package:f_access/components/help_button.dart';
import 'package:f_access/typo/typo_colors.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const BottomNavBar(),
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        slivers: [
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              title: SizedBox(
                height: 32,
                child: Image.asset("assets/images/banner.png"),
              ),
              titlePadding: const EdgeInsets.all(16),
              expandedTitleScale: 2,
            ),
            elevation: 0,
            backgroundColor: TypoColors.white1,
            expandedHeight: 64 * 1.5,
            stretch: true,
            collapsedHeight: 64,
            pinned: true,
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  const SizedBox(height: 16),
                  Wrap(
                    runSpacing: 16,
                    children: const [
                      HelpButton(
                        icon: Icons.security_rounded,
                        title: "CTEPO",
                        description: "Pedir ajuda da policia.",
                      ),
                      HelpButton(
                        icon: Icons.emergency_rounded,
                        title: "CARGY",
                        description: "Chamar ambulância.",
                      ),
                      HelpButton(
                        icon: Icons.fire_truck_rounded,
                        title: "CFIRE",
                        description: "Chamar corpo de bombeiros.",
                      ),
                      HelpButton(
                        icon: Icons.directions_bus_rounded,
                        title: "POBUS",
                        description: "Checar se o ônibus está chegando.",
                      ),
                      HelpButton(
                        icon: Icons.compare_rounded,
                        title: "OBJID",
                        description: "Identificar um objeto com a câmera.",
                      ),
                      HelpButton(
                        icon: Icons.my_location_rounded,
                        title: "LATIO",
                        description:
                            "Saber aonde ir, onde está, locais próximos, etc.",
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
