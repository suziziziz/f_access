import 'package:f_access/typo/typo_colors.dart';
import 'package:flutter/material.dart';

class Assistant extends StatelessWidget {
  const Assistant({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TypoColors.black1,
      body: Center(
        child: Hero(
          tag: "assistant-icon",
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 192,
                width: 192,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(96),
                  color: TypoColors.white6,
                ),
              ),
              ShaderMask(
                blendMode: BlendMode.srcIn,
                shaderCallback: (bounds) =>
                    TypoColors.gradientMain.createShader(bounds),
                child: const Icon(Icons.mic_rounded, size: 64),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
